import './Page.css';
import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';

function Page() {
  return (
    <>
      <Header />
      <Main />
      <Footer />
    </>
  );
}

export default Page;
