import React from 'react';

function Photo(props) {
    return (  
        <div className='photo'>
            <img alt={props.title} src={props.src} />
            <p>{props.title}</p>
        </div>
    );
}

export default Photo;