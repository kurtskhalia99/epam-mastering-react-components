import React from 'react';
import Photo from './Photo';
import PropTypes from 'prop-types';

import './Main.css';
import Photos from './Photos';


function Main() {
    return (
        <Photos />
    );
}


export default Main;