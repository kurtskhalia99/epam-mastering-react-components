import React from 'react';
import PropTypes from 'prop-types';


function Footer(props) {
    return (
        <div style={{color: 'blue'}}>
            <h3>{props.title}</h3>
        </div>
    );
}

Footer.defaultProps = {
    title: "Footer title"
}

Footer.propTypes ={
    title: PropTypes.string
}

export default Footer;