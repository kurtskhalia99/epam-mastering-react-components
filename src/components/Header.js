import React from 'react';
import PropTypes from 'prop-types';

function Header(props) {
    return (
        <div style={{height: 50, color: 'red'}}>
            <h1>{props.title}</h1>
        </div>
    );
}

Header.defaultProps = {
    title: "Header title"
}

Header.propTypes ={
    title: PropTypes.string
}


export default Header;